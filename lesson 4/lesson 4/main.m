
#import <Foundation/Foundation.h>
#import "Animal.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        Animal *tigr = [Animal createObjectWithName:@"Gladiolus"
                                            withAge:2
                                         withGender:YES];
        NSLog(@"%@", tigr);
    }
    return 0;
}

