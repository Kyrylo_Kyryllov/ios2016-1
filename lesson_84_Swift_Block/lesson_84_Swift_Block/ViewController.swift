//
//  ViewController.swift
//  lesson_84_Swift_Block
//
//  Created by Yurii Bosov on 12/28/16.
//  Copyright © 2016 ios. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var dataSources = [TerimalInfo]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DAL.sharedInstance.allTerminalsFor(cityName: "Dnipro", complition: {(data: [TerimalInfo]?, errorMessage: String?) in
            
            if data != nil {
                // show data (array of TerimalInfo)
                self.dataSources = data!
                self.collectionView.reloadData()
                
            } else {
                // show errorMessage
                print(errorMessage!)
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath)
        
        
        return cell
    }
}

