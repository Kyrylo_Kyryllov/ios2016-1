

#import <Foundation/Foundation.h>

@interface Monitor : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) CGFloat diagonal;

@end
