//
//  Catalog+CoreDataProperties.h
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Catalog+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Catalog (CoreDataProperties)

+ (NSFetchRequest<Catalog *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSNumber *identifier;
@property (nullable, nonatomic, copy) NSNumber *parentIdentifier;
@property (nullable, nonatomic, retain) NSSet<Product *> *products;
@property (nullable, nonatomic, retain) NSSet<Catalog *> *subCatalogs;
@property (nullable, nonatomic, retain) Catalog *parentCatalog;

@end

@interface Catalog (CoreDataGeneratedAccessors)

- (void)addProductsObject:(Product *)value;
- (void)removeProductsObject:(Product *)value;
- (void)addProducts:(NSSet<Product *> *)values;
- (void)removeProducts:(NSSet<Product *> *)values;

- (void)addSubCatalogsObject:(Catalog *)value;
- (void)removeSubCatalogsObject:(Catalog *)value;
- (void)addSubCatalogs:(NSSet<Catalog *> *)values;
- (void)removeSubCatalogs:(NSSet<Catalog *> *)values;

@end

NS_ASSUME_NONNULL_END
