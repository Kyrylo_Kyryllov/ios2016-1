//
//  ProductsController.h
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Catalog+CoreDataClass.h"
#import "Product+CoreDataClass.h"

@interface ProductsController : UITableViewController

@property (nonatomic, strong) Catalog *catalog;

@end
