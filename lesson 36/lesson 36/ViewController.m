//
//  ViewController.m
//  lesson 36
//
//  Created by Yuriy Bosov on 6/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.indicator.hidden = NO;
    
    self.bbiBack.enabled = NO;
    self.bbiForward.enabled = NO;
    self.bbiRefresh.enabled = NO;
    
    self.webView.backgroundColor = [UIColor whiteColor];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"htmlPage" ofType:@"html"];
//    NSString *htmlStr = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
//    html.Str = [NSString stringWithFormat:htmlStr, self.view.frame.size.width];
//    [self.webView loadHTMLString:htmlStr baseURL:nil];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
}

#pragma Actions

- (IBAction)backButtonDidPressed:(id)sender
{
    [self.webView goBack];
}

- (IBAction)forwardButtonDidPressed:(id)sender
{
    [self.webView goForward];
    
//    UIActivityViewController
}

- (IBAction)refreshButtonDidPressed:(id)sender
{
    [self.webView reload];
}

#pragma UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    
    self.bbiRefresh.enabled = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.indicator.hidden = YES;
    [self.indicator stopAnimating];
    
    self.bbiRefresh.enabled = YES;
    
    self.bbiBack.enabled = [self.webView canGoBack];
    self.bbiForward.enabled = [self.webView canGoForward];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.indicator.hidden = YES;
    [self.indicator stopAnimating];
    
    self.bbiRefresh.enabled = YES;
}

@end
