//
//  main.swift
//  lesson_80_Swift_0
//
//  Created by Yurii Bosov on 12/10/16.
//  Copyright © 2016 ios. All rights reserved.
//

import Foundation

// объвление переменных
// let - для констант
// var - для переиспользуемых переменных (т.е. для этих переменных мы можем переназначить значение)

// обьявляем константу i
let i = 10
// i = 20 - мы не можем так написать, так как мы обьявили ее как let (константу)
print("i = \(i)")   // примеры вывода используя \()
print("i =", i)     // примеры вывода через запятую

// обьявляем переменную j (целочисленная)
var j = 10
j = 22
print("j = \(j)")

// обьявляем переменную f (с плавающей точкой)
var f = 10.5
f = -10.5
print("f = \(f)")
print(NSString(format:"f = %0.2f",f)) // вывод с ограничением символов после запятой

// обьявляем переменную k
let k = j % 4
print("k = \(k)")


////////////////////// массивы
// объявление и инициализация массива строк
var array1 = ["a","b","c"]
print("arra1 =", array1)

// объявление массива строк (без инициализации, но с указанием типа объектов)
var array2 = [String]()
// добавление элемента в массив
array2.append("x")
print("arra2 = \(array2)")
// очистка массива
array2.removeAll(keepingCapacity: true)
print("arra2 = \(array2)")


////////////////////// словари
// объявление словари и инициализация
var dict1 = ["key1":1,"key2":2,"key3":3]
// получение значения по ключу
var value = dict1["key1"]
// установка нового значение по ключу
dict1["key1"] = 10
// добавление нового ключа\значение
dict1["key4"] = 40
print("dict1 =", dict1)

// объявление словари, без инициализации, но с указанием типов для ключа и значние (в даном случае ключ это строка, значение это целое число)
var dict2 = [String:Int]()
dict2[String(10)] = Int(true)
print("dict2 =", dict2)

////////////////////// enum
// объявление enum (указываем Имя и Тип)

enum MyEnum : Int {
    case myEnumValue1, myEnumValue2, myEnumValue3, myEnumValue4
    // внутри enum можно обьявить свою функцию
    func enumFunc() -> String {
        switch self {
        case .myEnumValue1:
            return "Нулевое значение энама"
        default:
            return String(self.rawValue)
        }
    }
}

var test = MyEnum.myEnumValue1
print("test =", test.enumFunc())


