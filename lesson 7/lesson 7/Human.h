//
//  Human.h
//  lesson 7
//
//  Created by Yuriy Bosov on 2/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Human : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) BOOL gender;

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender;

@end
