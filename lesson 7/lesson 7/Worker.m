//
//  Worker.m
//  lesson 7
//
//  Created by Yuriy Bosov on 2/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Worker.h"

@implementation Worker

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender
             withPlaceWork:(NSString *)placeWork
               withSallary:(NSUInteger)sallary
             withRetireAge:(NSUInteger)retireAge
{
    Worker *tempObj = [[self class] createObjectWithName:name
                                           withAge:age
                                        withGender:gender];
    
    tempObj.placeWork = placeWork;
    tempObj.sallary = sallary;
    tempObj.retireAge = retireAge;
    
    return tempObj;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, PlaceWork = %@, Sallary = %lu, Retire Age = %lu", [super description], self.placeWork, self.sallary, self.retireAge];
}

@end
