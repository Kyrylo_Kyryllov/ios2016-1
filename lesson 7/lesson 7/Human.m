//
//  Human.m
//  lesson 7
//
//  Created by Yuriy Bosov on 2/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Human.h"

@implementation Human

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender
{
    Human *tempObj = [[[self class] alloc] init];
    
    tempObj.name = name;
    tempObj.age = age;
    tempObj.gender = gender;
    
    return tempObj;
}

- (NSString *)description
{
    NSString *genderString = _gender == YES ? @"male" : @"female";
    
    return [NSString stringWithFormat:@"Name = %@, Age = %lu, Gender = %@", _name, _age, genderString];
}

@end
