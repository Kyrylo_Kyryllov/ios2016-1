//
//  MenuModel.h
//  lesson 43_SplipVeiwController
//
//  Created by Yuriy Bosov on 7/4/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *logo;
@property (nonatomic, strong) NSString *storyboardID;

+ (MenuModel *)modelWithTitle:(NSString *)title
                         logo:(NSString *)logo
                 storyboardID:(NSString *)storyboardID;

@end
