//
//  ContactBook.h
//  lesson 15_repeat_oop
//
//  Created by Yuriy Bosov on 3/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface ContactBook : NSObject
{
    NSMutableArray *contactsList;
}

- (void)addContact:(Contact *)contact;
- (void)removeContact:(Contact *)contact;
- (NSArray *)sortedContactsList;
- (NSArray *)findContactsByPhone:(NSString *)phone;
- (NSArray *)findContactsByName:(NSString *)name;

@end
