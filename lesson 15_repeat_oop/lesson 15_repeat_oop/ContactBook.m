//
//  ContactBook.m
//  lesson 15_repeat_oop
//
//  Created by Yuriy Bosov on 3/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ContactBook.h"

@implementation ContactBook

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        contactsList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addContact:(Contact *)contact
{
    if (![contactsList containsObject:contact])
        [contactsList addObject:contact];
}

- (void)removeContact:(Contact *)contact
{
    [contactsList removeObject:contact];
}

- (NSArray *)sortedContactsList
{
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    return [contactsList sortedArrayUsingDescriptors:@[sd]];
}

- (NSArray *)findContactsByPhone:(NSString *)phone
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (Contact *contact in contactsList)
    {
        for (NSString *ph in [contact phones])
        {
            NSRange range = [ph rangeOfString:phone];
            if (range.location != NSNotFound)
            {
                [result addObject:contact];
                break;
            }
        }
    }
    
    return result;
}

- (NSArray *)findContactsByName:(NSString *)name
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (Contact *contact in contactsList)
    {
        NSRange range = [contact.name rangeOfString:name];
        if (range.location != NSNotFound)
        {
            [result addObject:contact];
        }
    }
    
    return result;
}

@end
