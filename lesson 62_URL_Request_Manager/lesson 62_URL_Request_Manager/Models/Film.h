//
//  Film.h
//  lesson 62_URL_Request_Manager
//
//  Created by Yuriy Bosov on 10/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Film : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *nameRu;
@property (nonatomic, strong) NSString *nameEn;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *genre;

@property (nonatomic, strong) NSString *videoURL;
@property (nonatomic, strong) NSString *posterURL;

@property (nonatomic) BOOL is3D;
@property (nonatomic) BOOL isIMAX;

+ (Film *)filmWithData:(NSDictionary *)data;
+ (NSArray<Film *>*)filmsWithArrayData:(NSArray *)arrayData;

@end
