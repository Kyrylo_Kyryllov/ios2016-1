//
//  Film.m
//  lesson 62_URL_Request_Manager
//
//  Created by Yuriy Bosov on 10/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Film.h"

@interface NSDictionary (NonNull)

- (id)nonNullObjectForKey:(id)aKey;
- (id)nonNullObjectForKeyPath:(id)aKey;

@end

@implementation  NSDictionary (NonNull)

- (id)nonNullObjectForKey:(id)aKey {
    
    id obj = [self objectForKey:aKey];
    return [obj isKindOfClass:[NSNull class]] ? nil : obj;
}

- (id)nonNullObjectForKeyPath:(id)aKey {
    id obj = [self valueForKeyPath:aKey];
    return [obj isKindOfClass:[NSNull class]] ? nil : obj;
}

@end



@implementation Film

+ (Film *)filmWithData:(NSDictionary *)data {
    Film *model = [Film new];
    
    model.identifier = [data nonNullObjectForKey:@"id"];
    model.nameRu = [data nonNullObjectForKey:@"nameRU"];
    model.nameEn = [data nonNullObjectForKey:@"nameEN"];
    model.rating = [data nonNullObjectForKey:@"rating"];
    model.genre = [data nonNullObjectForKey:@"genre"];
    
    model.posterURL = [data nonNullObjectForKey:@"posterURL"];
    model.videoURL = [data nonNullObjectForKeyPath:@"videoURL.sd"];
    
    model.isIMAX = [[data nonNullObjectForKey:@"isIMAX"] boolValue];
    model.is3D = [[data nonNullObjectForKey:@"is3D"] boolValue];
    
    return model;
}

+ (NSArray<Film *>*)filmsWithArrayData:(NSArray *)arrayData{

    NSMutableArray *result = [NSMutableArray array];
    
    for (NSDictionary *data in arrayData) {
        [result addObject:[Film filmWithData:data]];
    }
    
    return result;
}

@end
