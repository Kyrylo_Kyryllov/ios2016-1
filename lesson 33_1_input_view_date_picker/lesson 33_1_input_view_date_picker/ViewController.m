//
//  ViewController.m
//  lesson 33_1_input_view_date_picker
//
//  Created by Yuriy Bosov on 5/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
    
    tfDateOfBirth.inputView = datePicker;
    tfMorningTime.inputView = datePicker;
    tfNightTime.inputView = datePicker;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePicker)];
    [self.view addGestureRecognizer:recognizer];
    
    dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"EEEE, dd MMMM yyyy";
    dateFormater.locale = datePicker.locale;
    
    timeFormater = [[NSDateFormatter alloc] init];
    timeFormater.dateFormat = @"HH:mm";
    timeFormater.locale = datePicker.locale;
}

#pragma mark - Actions

- (void)hidePicker
{
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //... сохраняем дату и отображем ее в полях ввода
    if (textField == tfDateOfBirth)
    {
        dateOfBirth = datePicker.date;
        tfDateOfBirth.text = [dateFormater stringFromDate:dateOfBirth];
    }
    else if (textField == tfMorningTime)
    {
        morningTime = datePicker.date;
        tfMorningTime.text = [timeFormater stringFromDate:morningTime];
    }
    else if (textField == tfNightTime)
    {
        nightTime = datePicker.date;
        tfNightTime.text = [timeFormater stringFromDate:nightTime];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //... конфигурируем datePicker
    if (textField == tfDateOfBirth)
    {
        datePicker.datePickerMode = UIDatePickerModeDate;
        if (dateOfBirth)
            datePicker.date = dateOfBirth;
    }
    else
    {
        datePicker.datePickerMode = UIDatePickerModeTime;
        if (textField == tfMorningTime && morningTime)
            datePicker.date = morningTime;
        if (textField == tfNightTime && nightTime)
            datePicker.date = nightTime;
    }
}

@end
