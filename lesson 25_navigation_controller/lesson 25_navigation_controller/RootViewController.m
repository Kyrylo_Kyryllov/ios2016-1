//
//  ViewController.m
//  lesson 25_navigation_controller
//
//  Created by Yuriy Bosov on 4/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Root View Controller";
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
