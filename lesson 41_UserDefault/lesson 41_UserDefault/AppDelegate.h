//
//  AppDelegate.h
//  lesson 41_UserDefault
//
//  Created by Yuriy Bosov on 6/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

