//
//  InternetShop.h
//  lesson 14_repeat_oop
//
//  Created by Yuriy Bosov on 3/14/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface InternetShop : NSObject
{
    NSString *name;
    NSMutableDictionary *products;
}

- (id)initWithName:(NSString *)name;

- (void)addProduct:(Product *)product;
- (void)removeProduct:(Product *)product;

- (void)showInfoShop;
- (void)showCategoryInfo;

- (NSString *)stringByCount:(NSUInteger)count;

- (NSArray *)findProductsByName:(NSString *)productName;
- (NSArray *)findProductsByCategoryName:(NSString *)categoryName;
- (NSArray *)findProductsByTag:(NSString *)tag;
- (NSArray *)findProductsByCharacteristic:(NSString *)characteristic;

@end
