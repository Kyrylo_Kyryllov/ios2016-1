//
//  main.m
//  lesson 8
//
//  Created by Yuriy Bosov on 2/17/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
//        for (NSUInteger i = 0; i < 4; i++)
//        {
//            NSLog(@"i = %lu",i);
//            
//            if (i >= 1)
//            {
//                continue;
//            }
//            
//            NSLog(@"Hello, World!");
//        }
        
//        NSUInteger i = 10;
//        while (i < 10)
//        {
//            NSLog(@"i = %lu", i);
//            i++;
//        }
        NSUInteger i = 10;
        
        do
        {
            NSLog(@"i = %lu", i);
            i++;
        }
        while (i < 10);
    }
    return 0;
}
