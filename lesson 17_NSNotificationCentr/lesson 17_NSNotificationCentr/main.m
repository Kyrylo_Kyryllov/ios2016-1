//
//  main.m
//  lesson 17_NSNotificationCentr
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sender.h"
#import "Reciever.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        Sender *sender = [[Sender alloc] init];
        Reciever *reciever = [[Reciever alloc] init];
        
        NSLog(@"sender = %@", sender);
        NSLog(@"reciever = %@", reciever);
        
        [sender sendMessage];
    }
    return 0;
}
