//
//  Dispatcher.m
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Dispatcher.h"
#import "Truck.h"

@implementation Dispatcher

- (NSString *)destinationOfTruck:(Truck *)aTruck
{
    if ([aTruck.truckName isEqualToString:@"VOLVO"])
    {
        return @"Киев";
    }
    else
    {
        return @"Львов";
    }
}

- (Cargo)typeCargoOfTruck:(Truck *)aTruck
{
    if ([aTruck.truckName isEqualToString:@"VOLVO"])
    {
        return cargoContainer;
    }
    else
    {
        return cargoAutoTrailer;
    }
}

@end
