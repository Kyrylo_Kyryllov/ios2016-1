//
//  ViewController.m
//  lesson 35_2_segmented_controll
//
//  Created by Yuriy Bosov on 6/1/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
    [UISegmentedControl appearance].tintColor = [UIColor greenColor];
}

- (IBAction)segmentedControllAction:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0)
    {
        self.view.backgroundColor = [UIColor redColor];
        sender.tintColor = [UIColor greenColor];
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        self.view.backgroundColor = [UIColor greenColor];
        sender.tintColor = [UIColor yellowColor];
    }
    else
    {
        self.view.backgroundColor = [UIColor blueColor];
        sender.tintColor = [UIColor whiteColor];
    }
}

@end
