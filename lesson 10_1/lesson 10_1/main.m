
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        // класс-коллекция "словарь" (NSDictionary)
        
        // создание словаря
        // с помощью метода +dictionaryWithObjectsAndKeys:
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"object1",@"key1",@"object2",@"key2", nil];
        NSLog(@"%@", dict);
        
        // с помощью метода +dictionaryWithObjects:forKeys:
        dict = [NSDictionary dictionaryWithObjects:@[@(100),@(200),@(-300)]
                                           forKeys:@[@"key1",@"key2",@"key3"]];
        NSLog(@"%@", dict);
        
        // с помощью сокращенной записи
        dict = @{@"key1": @"obj1",
                 @"key2": @"obj2",
                 @"key3": @"obj3"};
        NSLog(@"%@", dict);
        
        ////////////////////////////////////////////////////////////
        // доступ к элементам словаря
        // досуп осуществляется по клучю (key)
        id object = [dict objectForKey:@"key0"];
        NSLog(@"object = %@",object);
        // если по данному ключу нет объекта, то метод objectForKey: вернет nill
        
        // доступ к элементам словаря с помощью сокращенной записи
        object = dict[@"key1"];
        NSLog(@"object = %@",object);
        
        // получение списка всех ключей словаря
        NSArray *allKeys = [dict allKeys];
        NSLog(@"allKeys = %@", allKeys);
        
        // получение списка всех объектов словаря
        NSArray *allValues = [dict allValues];
        NSLog(@"allValues = %@", allValues);
        
        // перебор элементов словаря с помощью цикла
        for (NSString *key in [dict allKeys])
        {
            id object = [dict objectForKey:key];
            NSLog(@"object = %@", object);
        }
    }
    return 0;
}
