//
//  main.m
//  lesson_79_Localizations
//
//  Created by Yurii Bosov on 12/5/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
