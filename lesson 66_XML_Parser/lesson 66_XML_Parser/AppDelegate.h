//
//  AppDelegate.h
//  lesson 66_XML_Parser
//
//  Created by Yurii Bosov on 10/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

