//
//  ViewController.m
//  lesson_76_NSFileManager
//
//  Created by Yurii Bosov on 11/23/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)createFolderInDocumentDirectory:(id)sender {
    // для работы в файловой системой используем NSFileManager;
    
    // имя папки, которую я хочу создать
    NSString *folderName = @"MyFolderInDirectoriesDir";
    
    // 1. получаем путь на папку Documents
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    
    // 2. проверяем, а небыла ли создана эта папку ранее
    // 2.1 создаем переменую, в которой храним полный путь
    NSString *fullPathToMyFolder = [path stringByAppendingPathComponent:folderName];
    BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:fullPathToMyFolder];

    if (result) {
        NSLog(@"папку уже создана");
    }else {
        NSLog(@"папка не создана, будем создавать");
        NSError *error = nil;
        // создаем папку
        BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:fullPathToMyFolder withIntermediateDirectories:NO attributes:nil error:&error];
        if (result && !error) {
            NSLog(@"папка была создана");
        } else {
            NSLog(@"папка не создана, ошибка %@", error);
        }
    }
}

- (IBAction)createFolderInCasheDirectory:(id)sender {
    
    // имя папки, которую я хочу создать
    NSString *folderName = @"MyFolderInDirectoriesDir";
    
    // 1. получаем путь на папку Cache
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    
    // 2. проверяем, а небыла ли создана эта папку ранее
    // 2.1 создаем переменую, в которой храним полный путь
    NSString *fullPathToMyFolder = [path stringByAppendingPathComponent:folderName];
    BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:fullPathToMyFolder];
    
    if (result) {
        NSLog(@"папку уже создана");
    }else {
        NSLog(@"папка не создана, будем создавать");
        NSError *error = nil;
        // создаем папку
        BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:fullPathToMyFolder withIntermediateDirectories:NO attributes:nil error:&error];
        if (result && !error) {
            NSLog(@"папка была создана");
        } else {
            NSLog(@"папка не создана, ошибка %@", error);
        }
    }
}

- (IBAction)createFolderInTempDirectory:(id)sender {
    
    // имя папки, которую я хочу создать
    NSString *folderName = @"MyFolderInDirectoriesDir";
    
    // 1. получаем путь на папку Temp
    NSString *path = NSTemporaryDirectory();
    
    // 2. проверяем, а небыла ли создана эта папку ранее
    // 2.1 создаем переменую, в которой храним полный путь
    NSString *fullPathToMyFolder = [path stringByAppendingPathComponent:folderName];
    BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:fullPathToMyFolder];
    
    if (result) {
        NSLog(@"папку уже создана");
    }else {
        NSLog(@"папка не создана, будем создавать");
        NSError *error = nil;
        // создаем папку
        BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:fullPathToMyFolder withIntermediateDirectories:NO attributes:nil error:&error];
        if (result && !error) {
            NSLog(@"папка была создана");
        } else {
            NSLog(@"папка не создана, ошибка %@", error);
        }
    }
}

- (IBAction)createFile:(id)sender {
    NSString *fileName = @"somethingFile.plist";
    //1 get path to directiory
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *fullPath = [path stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fullPath]){
        
        // создаем pist на основе dictionary
        NSDictionary *dictionary = @{@"key1":@"value1",
                                     @"key2":@"value2",
                                     @"key3":@"value3"};
        
        // create file
        BOOL create = [[NSFileManager defaultManager] createFileAtPath:fullPath contents:nil attributes:nil];
        if (create) {
            // записываем в файл
            BOOL write =[dictionary writeToFile:fullPath atomically:YES];
            if (write) {
                NSLog(@"plist файл создан");
            } else {
                NSLog(@"plist файл не создан");
            }
        }
        
    }
}

- (IBAction)readFile:(id)sender {
    NSString *fileName = @"somethingFile.plist";
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *fullPath = [path stringByAppendingPathComponent:fileName];
    
    // проверяем есть ли файл
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath]){
        
        // создаем словарь из плиста
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:fullPath];
        NSLog(@"файл прочитан\n%@", dictionary);
        
    } else {
        NSLog(@"файл %@ не найден", fileName);
    }
}

- (IBAction)deleteFile:(id)sender {
    NSString *fileName = @"somethingFile.plist";
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *fullPath = [path stringByAppendingPathComponent:fileName];
    
    // проверяем есть ли файл
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath]){
        // удаляем файл
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:fullPath
                                                   error:&error];
        if (error) {
            NSLog(@"не удалось удалить файл");
        }else {
            NSLog(@"файл %@ был удален", fileName);
        }
        
    } else {
        NSLog(@"файл %@ не найден", fileName);
    }
}

- (IBAction)moveFile:(id)sender {
    NSString *fileName = @"somethingFile.plist";
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *fullPath = [path stringByAppendingPathComponent:fileName];
    
    // проверяем есть ли файл
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath]){
        // перещаем файл в другую папку
        NSString *toPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        NSString *fullToPath = [toPath stringByAppendingPathComponent:fileName];
        
        NSError *error = nil;
        [[NSFileManager defaultManager] moveItemAtPath:fullPath
                                                toPath:fullToPath
                                                 error:&error];
        if (error) {
            NSLog(@"не удалось переместить файл");
        }else {
            NSLog(@"файл %@ был перемещен", fileName);
        }
        
    } else {
        NSLog(@"файл %@ не найден", fileName);
    }
}

@end
