//
//  AppDelegate.h
//  lesson_76_NSFileManager
//
//  Created by Yurii Bosov on 11/23/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

