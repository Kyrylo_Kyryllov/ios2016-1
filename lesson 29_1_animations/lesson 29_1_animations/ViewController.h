//
//  ViewController.h
//  lesson 29_1_animations
//
//  Created by Yuriy Bosov on 5/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *circle;


@end

