//
//  NoteCell.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "NoteCell.h"

@interface NoteCell ()

+ (NSDateFormatter *)dateFormatter;

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbDate;
@property (nonatomic, weak) IBOutlet UILabel *lbBody;

@end

@implementation NoteCell

+ (NSDateFormatter *)dateFormatter{
   
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MM yyyy, HH mm";
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
    });
    
    return dateFormatter;
}

- (void)setupNote:(Note *)note {
    
    self.note = note;
    
    self.lbTitle.text = note.title;
    self.lbBody.text = note.body;
    self.lbDate.text = [[NoteCell dateFormatter] stringFromDate:note.modifyDate];
}

@end
