//
//  GameItem.h
//  lesson 48_game_sapper
//
//  Created by Yuriy Bosov on 7/20/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    GameItemStateClose,
    GameItemStateOpen
} GameItemState;

@interface GameItem : NSObject

@property (nonatomic) CGRect rect;
@property (nonatomic) GameItemState state;
@property (nonatomic) BOOL hasBomb;
@property (nonatomic) NSUInteger nearbyBombsCount;
@property (nonatomic) NSUInteger numer;

@end
