
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        // арифметичесике операции
        NSInteger a = 500;
        NSInteger b = 200;
        
        NSInteger c = a + b;
        NSLog(@"c = a + b; c = %li",c);
       
        c = a - b;
        NSLog(@"c = a - b; c = %li",c);
        
        c = a * b;
        NSLog(@"c = a * b; c = %li",c);
        
        c = a / b;
        NSLog(@"c = a / b; c = %li",c);
        
        // взятие остатка от деления
        c = a % b;
        NSLog(@"c = a %% b; c = %li",c);
    }
    return 0;
}
