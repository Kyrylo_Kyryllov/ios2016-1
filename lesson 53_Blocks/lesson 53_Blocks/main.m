
#import <Foundation/Foundation.h>
#import "CustomClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSLog(@"start");
        
        //========================================
        // объявление блока,
        // 1. блок ничего не возвращает, ничего не принимает
        void (^block)(void);
        // инициализация блока
        block = ^(void){
            NSLog(@"код блока void^(void)");
        };
        // вызов кода, который был в блоке
        block();
        
        // 2. блок с параметрами, который ничего не возвращает
        void (^blockWithParam)(NSString *) = ^(NSString *str) {
            NSLog(@"код блока void^(NSString*), %@", str);
        };
        // вызов блока
        blockWithParam(@"bla-bla-bla");
        
        void (^blockWithParams)(NSString *, NSInteger, BOOL) = ^(NSString *str, NSInteger i, BOOL b) {
            NSLog(@"код блока void^(NSString *, NSInteger, BOOL), %@, %li, %i", str, i, b);
        };
        // вызов блока
        blockWithParams(@"bla-bla", 100, YES);
        
        // 3. блок с возвращаемым значением
        NSString * (^blockWithReturnValue)(void) = ^(void){
            NSLog(@"код блока NSString *^(void)");
            return @"retunt value";
        };
        // вызов блока
        NSString *s = blockWithReturnValue();
        NSLog(@"%@",s);
        
        // 4. блок с возвращаемым значением и параметром
        BOOL (^blockWithReturnValueAndParams)(NSInteger) = ^(NSInteger i) {
            NSLog(@"код блока BOOL ^(NSInteger)");
            BOOL result = (i%2 == 0);
            return result;
        };
        // вызов метода
        BOOL b = blockWithReturnValueAndParams(10);
        NSLog(@"%i", b);
        
        //========================================
        // блок и внешнии переменные
        __block NSInteger i = 100;
        __block NSString *str = @"какая то строка";
        CustomClass *obj = [CustomClass new];
        
        block = ^(void){
            
            NSLog(@"i = %li", i);
            NSLog(@"str = %@", str);
            NSLog(@"obj = %@", obj);
            
            // в блоке можно менять значение переменным, для этого при объявлении переменной нужно указать __block
            i = 10;
            str = @"новое значение для переменной str";
            
        };
        block();
        
        NSLog(@"i = %li", i);
        NSLog(@"str = %@", str);
        
        //========================================
        // кастомные блоки
        CustomBlock customBlock = ^(NSString *str, NSInteger i, BOOL b) {
            NSLog(@"код блока void^(NSString *, NSInteger, BOOL), %@, %li, %i", str, i, b);
        };
        customBlock(@"str", 100, NO);
        
        // фикс утечки памяти, когда в блоке объекта нужно заюзать сам объект
        __weak typeof(obj) weakObj = obj;
        
        obj.block1 = ^(NSString *str, NSInteger i, BOOL b) {
            NSLog(@"obj = %@", weakObj);
        };
        
        [obj customMethodWithBlock:^(NSString *str, NSInteger i, BOOL b) {
            //
        }];
        
        [obj customMethod];
        
        NSLog(@"end");
    }
    return 0;
}
