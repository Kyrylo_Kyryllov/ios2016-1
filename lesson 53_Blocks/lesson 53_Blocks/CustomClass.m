//
//  CustomClass.m
//  lesson 53_Blocks
//
//  Created by Yuriy Bosov on 9/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CustomClass.h"

@implementation CustomClass

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)customMethod {
    
    __weak typeof(self) weakSelf = self;
    
    self.block1 = ^(NSString *str, NSInteger i, BOOL b) {
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        strongSelf->intValue = 100;
        weakSelf.name = @"new name";
        
    };
}

- (void)customMethodWithBlock:(CustomBlock)block {
    block(@"qqqqqq", 10000, YES);
}

@end
