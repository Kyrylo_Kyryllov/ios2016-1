//
//  ResultTableViewController.h
//  lesson 38_search_controller
//
//  Created by Yuriy Bosov on 6/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ResultTableViewController;

@protocol ResultTableViewProtocol <NSObject>

- (void)resultTableViewController:(ResultTableViewController *)resultViewController didSelectItem:(id)item;

@end


@interface ResultTableViewController : UITableViewController

@property (nonatomic, weak) id<ResultTableViewProtocol> delegate;

- (void)reloadDataSource:(NSArray *)aDataSource;

@end
