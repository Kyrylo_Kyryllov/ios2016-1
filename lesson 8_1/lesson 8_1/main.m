//
//  main.m
//  lesson 8_1
//
//  Created by Yuriy Bosov on 2/17/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        // объявление массива
        NSArray *array = nil;
        
        // инициализация массива, состоящего из трех строковых элементов
        array = [NSArray arrayWithObjects:@"qwer",@"asdf",@"zxcv", nil];
        NSLog(@"%@", array);
        
        // сокращенная запись создания массива (!)БЕЗ 'nil' В КОНЦЕ
        array = @[@"12",@"123",@"1234"];
        NSLog(@"%@",array);
        
        // создание массива с строковым, двумя числовыми и одним булевским значением
        array = @[@"Yuriy",@(30),@(70),@(YES)];
        
        array = [NSArray arrayWithObjects:
                 @"Yuriy",
                 [NSNumber numberWithUnsignedInteger:30],
                 [NSNumber numberWithUnsignedInteger:70],
                 [NSNumber numberWithBool:YES],
                 nil];
        
        NSLog(@"%@",array);
    }
    return 0;
}
