//
//  AppDelegate.h
//  lesson 47_2_core_graphics_UIBezierPath
//
//  Created by Yuriy Bosov on 7/18/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

