//
//  DrawView.m
//  lesson 47_2_core_graphics_UIBezierPath
//
//  Created by Yuriy Bosov on 7/18/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DrawView.h"

//typedef enum : NSUInteger {
//    CellStatusNone,
//    CellStatusX,
//    CellStatusO,
//} CellStatus;
//
//@interface GameCell : NSObject
//
//@property (nonatomic, strong) NSValue *rectValue;
//@property (nonatomic, assign) CellStatus status;
//
//@end
//
//@implementation GameCell
//
//@end

@interface NSArray (MY_Categories)

@property (nonatomic, strong) NSString *test;

- (NSArray *)swapValues;

@end


@implementation NSArray (MY_Categories)

@dynamic test;

- (NSArray *)swapValues {
    
    
    return [NSArray new];
}

@end


@implementation DrawView

- (void)drawRect:(CGRect)rect {
    
    CGRect rect1;
    CGRect rect2;
    
    NSArray *arr = @[[NSValue valueWithCGRect:rect1],
                     [NSValue valueWithCGRect:rect2]];
    
    arr = [arr swapValues];
    arr.test = @"adfadw";
    
    CGPoint p;
    
    for (NSValue *value in arr) {
        CGRect rect = [value CGRectValue];
        
        if (CGRectContainsPoint(rect, p))
        {
            // draw x or o
        }
    }
    
    [[UIColor redColor] setStroke];
    [[UIColor yellowColor] setFill];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 10;
    path.lineJoinStyle = kCGLineJoinRound;
    
    [path moveToPoint:CGPointMake(20, 20)];
    [path addLineToPoint:CGPointMake(100, 20)];
    [path addLineToPoint:CGPointMake(20, 200)];
    [path addLineToPoint:CGPointMake(100, 200)];
    [path closePath];
    
    [path stroke];
    [path fill];
}

@end
