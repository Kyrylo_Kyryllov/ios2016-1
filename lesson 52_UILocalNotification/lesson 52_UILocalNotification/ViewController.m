//
//  ViewController.m
//  lesson 52_UILocalNotification
//
//  Created by Yuriy Bosov on 9/7/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

- (IBAction)createLocalPushButtonClicked:(id)sender {
    
    // создание пуша
    UILocalNotification *notification = [UILocalNotification new];
    notification.timeZone  = [NSTimeZone systemTimeZone];
    notification.fireDate  = [[NSDate date] dateByAddingTimeInterval:5.0f];
    
    notification.alertAction = @"Test alert";
    notification.alertTitle = @"Test title";
    notification.alertBody = @"Test message";
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.applicationIconBadgeNumber = 1;
    
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}

@end
