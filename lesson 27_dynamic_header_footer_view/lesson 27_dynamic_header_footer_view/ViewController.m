//
//  ViewController.m
//  lesson 27_dynamic_header_footer_view
//
//  Created by Yuriy Bosov on 4/20/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.headerText.text = @"qweasd rqwer qwe qwerqwer qwe qwe rqwer qwe qwerqwer qwe qwer  qwer qwe qwerqwer qwe qwerq wer qwe qwe rqwer qwe qwerqwer qwe qwerqwer qwe qwerqwer qwe qwer qwer qwe qwer qwer qwe qwerqwer qwe qwerqw er qwe qw erqwer qwe qwer qwer qwe qwer qwer qwe qwerqwer qwe qwe rqwer qwe ";
    self.footerText.text = @"qwerqwer qwe qwerqwer qwe qwerqwer qwe qwerqwer qwe qwerqwer qwe qwerqwe";

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resizeHeaderViewForWidht:self.view.frame.size.width];
    [self resizeFooterViewForWidht:self.view.frame.size.width];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%li", indexPath.row];
    
    return cell;
}

#pragma mark - Resize

- (void)resizeHeaderViewForWidht:(CGFloat)widht
{
    // 0. занулить размер header View (иначе оно не корректно пересчитаем размер!!!)
    self.headerView.frame = CGRectZero;
    
    // 1. вычисляем размер контента heardView
    CGSize size = [self.headerView systemLayoutSizeFittingSize:CGSizeMake(widht, 0) withHorizontalFittingPriority:UILayoutPriorityDefaultHigh verticalFittingPriority:UILayoutPriorityDefaultLow];
    
    // 2. обновляем размеры у headerView, с учетом новый высоты (ее мы возьмем из size, который только что вычислили)
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    self.headerView.frame = frame;
    
    // 3. переназначаем таблице обновленную headerView
    self.tableView.tableHeaderView = self.headerView;
}

- (void)resizeFooterViewForWidht:(CGFloat)widht
{
    // 0. занулить размер header View (иначе оно не корректно пересчитаем размер!!!)
    self.footerView.frame = CGRectZero;
    
    // 1. вычисляем размер контента heardView
    CGSize size = [self.footerView systemLayoutSizeFittingSize:CGSizeMake(widht, 0) withHorizontalFittingPriority:UILayoutPriorityDefaultHigh verticalFittingPriority:UILayoutPriorityDefaultLow];
    
    // 2. обновляем размеры у headerView, с учетом новый высоты (ее мы возьмем из size, который только что вычислили)
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    self.footerView.frame = frame;
    
    // 3. переназначаем таблице обновленную headerView
    self.tableView.tableFooterView = self.footerView;
}

#pragma Rotate

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

// метод вызыватеся при изминении размера viewController-а, к при повороте экрана
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self resizeHeaderViewForWidht:size.width];
    [self resizeFooterViewForWidht:size.width];
}

@end
