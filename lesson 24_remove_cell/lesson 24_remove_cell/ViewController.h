//
//  ViewController.h
//  lesson 24_remove_cell
//
//  Created by Yuriy Bosov on 4/13/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

