//
//  Model.h
//  lesson 40_collectionView
//
//  Created by Yuriy Bosov on 6/22/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Model : NSObject

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat aspectRatio;

@end
