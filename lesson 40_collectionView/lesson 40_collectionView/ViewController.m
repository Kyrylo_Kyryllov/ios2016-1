//
//  ViewController.m
//  lesson 40_collectionView
//
//  Created by Yuriy Bosov on 6/22/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
#import "Model.h"

#define offset 5
#define column_count 4

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *dataArray;
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [[NSMutableArray alloc] init];
    
//    NSArray *aspectRatio = @[@(1),@(1.25),@(1.5),@(2)];
    
    for (int i = 0; i < 13; i++) {
        Model *model = [[Model alloc] init];
        
        // set color
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        
        model.color = color;
        
        // set aspect ratio
//        NSUInteger randomIndex = arc4random() % [aspectRatio count];
//        model.aspectRatio = [[aspectRatio objectAtIndex:randomIndex] floatValue];
        model.aspectRatio = 1;
        
        [dataArray addObject:model];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UICollectionViewCell *)sender {
    segue.destinationViewController.view.backgroundColor = sender.backgroundColor;
}

#pragma mark - Rotations

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return dataArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.label.text = [NSString stringWithFormat:@"cell %lu", indexPath.item];

    Model *model = [dataArray objectAtIndex:indexPath.item];
    cell.backgroundColor = model.color;
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
        
    UICollectionReusableView *result = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        result = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    }
    else if (kind == UICollectionElementKindSectionFooter)
    {
        result = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footerView" forIndexPath:indexPath];
    }
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat collectionViewWidth = collectionView.frame.size.width;
    
    UIEdgeInsets edgeInsets = [self collectionView:collectionView layout:collectionViewLayout insetForSectionAtIndex:indexPath.section];
    
    CGFloat itemWidth = (collectionViewWidth - (column_count - 1) * [self collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:indexPath.section] - edgeInsets.left - edgeInsets.right) / column_count;
    
    Model *model = [dataArray objectAtIndex:indexPath.item];
    
    return CGSizeMake(itemWidth, itemWidth * model.aspectRatio);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return offset;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)sectionsection {
    return offset;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(offset, offset, offset, offset);
}

@end
