//
//  AppDelegate.h
//  lesson 49_contacts_access
//
//  Created by Yuriy Bosov on 8/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

