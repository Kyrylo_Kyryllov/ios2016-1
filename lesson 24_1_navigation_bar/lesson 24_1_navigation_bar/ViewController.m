//
//  ViewController.m
//  lesson 24_1_navigation_bar
//
//  Created by Yuriy Bosov on 4/13/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (id)initWithCoder:(NSCoder *)qwr
{
    self = [super initWithCoder:qwr];
    if (self)
    {
        self.title = @"Custom title";
    }
    return self;
}

- (IBAction)editButtonPressed:(id)sender
{
    NSLog(@"editButtonPressed");
}

- (IBAction)addButtonPressed:(id)sender
{
    NSLog(@"addButtonPressed");
}

@end
