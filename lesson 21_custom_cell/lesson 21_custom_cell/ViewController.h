
#import <UIKit/UIKit.h>
#import "TableViewCellClick.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TableViewCellClickProtocol>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

