//
//  AppDelegate.h
//  lesson 61_URL_request
//
//  Created by Yuriy Bosov on 10/3/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

NSManagedObjectContext *context();
void saveContext();

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

