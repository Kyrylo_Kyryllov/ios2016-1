//
//  main.m
//  lesson 65 MapKit
//
//  Created by Vlad on 17.10.16.
//  Copyright © 2016 STEP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
