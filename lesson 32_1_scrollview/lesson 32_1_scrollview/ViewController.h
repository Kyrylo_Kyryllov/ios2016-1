//
//  ViewController.h
//  lesson 32_1_scrollview
//
//  Created by Yuriy Bosov on 5/23/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *lb1;
@property (nonatomic, weak) IBOutlet UILabel *lb2;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollVeiw;

@end

