//
//  CustomThread.m
//  lesson 54_1_Threads_1
//
//  Created by Yuriy Bosov on 9/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CustomThread.h"

@implementation CustomThread

- (void)main {
    
    NSTimeInterval startTime = [NSDate date].timeIntervalSince1970;
    
    NSInteger i = 0;
    for (; i < 1000000000; i++)
    {
        if ([self isCancelled]) {
            break;
        }
    }

    NSTimeInterval endTime = [NSDate date].timeIntervalSince1970;
    
    NSLog(@"%@ finished with cancel %i, count = %li, time duration %f",self.name, self.isCancelled, i, endTime - startTime);
}

@end
