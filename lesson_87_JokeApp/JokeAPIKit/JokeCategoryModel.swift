//
//  JokeCategoryModel.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class JokeCategoryModel: Any {

    public var site: String?
    public var name: String?
    public var url: String?
    public var parsel: String?
    public var encoding: String?
    public var linkpar: String?
    public var desc: String?
    
    public init(dict:[String:String]) {
        self.site = dict["site"]
        self.name = dict["name"]
        self.url = dict["url"]
        self.parsel = dict["parsel"]
        self.encoding = dict["encoding"]
        self.linkpar = dict["linkpar"]
        self.desc = dict["desc"]
    }
    
    public static func loadJokesCategories(complition: @escaping(_ data: [JokeCategoryModel]?, _ errorMessage: String?) ->()) {
        let url = URL(string:"http://www.umori.li/api/sources")
        
        let task = URLSession.shared.dataTask(with: url!) {(data: Any?, responce: URLResponse?, error: Error?) in
            
            if let rawData = data as? Data {
                
                do {
                    
                    let jsonData = try JSONSerialization.jsonObject(with: rawData, options: .allowFragments) as! [[[String:String]]]
                    
                    var categories = [JokeCategoryModel]()
                    
                    for array in jsonData {
                        for dict in array {
                            categories.append(JokeCategoryModel.init(dict: dict))
                        }
                    }
                    
                    DispatchQueue.main.async {
                        complition(categories, nil)
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        complition(nil, "Ошибка парсера!!!")
                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    complition(nil, error?.localizedDescription)
                }
            }
        }
        task.resume()
    }
}
