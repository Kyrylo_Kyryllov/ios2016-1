//
//  JokeAPIKit.h
//  JokeAPIKit
//
//  Created by Yurii Bosov on 1/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JokeAPIKit.
FOUNDATION_EXPORT double JokeAPIKitVersionNumber;

//! Project version string for JokeAPIKit.
FOUNDATION_EXPORT const unsigned char JokeAPIKitVersionString[];

//FOUNDATION_EXPORT Class JokeModel;

// In this header, you should import all the public headers of your framework using statements like #import <JokeAPIKit/PublicHeader.h>
