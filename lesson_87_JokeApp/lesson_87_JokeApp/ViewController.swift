//
//  ViewController.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import JokeAPIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, JokeCellProtocol {
    
    var currentSelectedModelView: JokeModelView?
    var currentSelectedIndexPath: IndexPath?
    
    var dataSource = [JokeModelView]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.activity.startAnimating()
        
        JokeModel.loadJokes(count: 100) { (data: [JokeModel]?, error: String?) in
            
            self.activity.stopAnimating()
            
            if let dataModels = data as [JokeModel]! {
                
                for model in dataModels {
                    let modelView = JokeModelView()
                    modelView.model = model
                    self.dataSource.append(modelView)
                }
                self.tableView.reloadData()
                
            } else {
                // show error aletr
                let alert = UIAlertController.init(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! JokeCell
        
        cell.setup(modelView: dataSource[indexPath.row])
        cell.selectionStyle = .none
        cell.delegate = self
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if dataSource[indexPath.row].showAllContent {
            return UITableViewAutomaticDimension
        }
        else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dataSource[indexPath.row].showAllContent {
            return UITableViewAutomaticDimension
        }
        else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let modelView = dataSource[indexPath.row]
        
        if currentSelectedIndexPath != nil &&
            currentSelectedModelView != nil{
            
            if currentSelectedIndexPath?.row == indexPath.row {
               currentSelectedModelView?.showAllContent = !(currentSelectedModelView?.showAllContent)!
            } else {
                currentSelectedModelView?.showAllContent = false
                currentSelectedModelView = modelView
                currentSelectedModelView?.showAllContent = true
            }
            
        } else {
            
            currentSelectedModelView = modelView
            currentSelectedModelView?.showAllContent = true
        }
        
        
        if currentSelectedIndexPath != nil {
        tableView.reloadRows(at: [indexPath, currentSelectedIndexPath!], with: UITableViewRowAnimation.automatic)
        } else {
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        currentSelectedIndexPath = indexPath
    }
    
    //MARK: - JokeCellProtocol
    func jokeCellDidSharedButtonClicked(cell: JokeCell) {
        // open sher controller
//        print("\(cell.modelView?.model.shareURL())")
        
        let shareController = UIActivityViewController.init(activityItems: [(cell.modelView?.model.shareURL())!], applicationActivities: nil)
        
        self.present(shareController, animated: true, completion: nil)
    }
}

