//
//  SettingsController.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import JokeAPIKit

class SettingsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var dataSources = [JokeCategoryModel]()
    
    @IBOutlet weak var offlineSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activity.startAnimating()
        
        JokeCategoryModel.loadJokesCategories { (data:[JokeCategoryModel]?, error: String?) in
            self.activity.stopAnimating()
            
            if let dataModels = data as [JokeCategoryModel]! {
                
                self.dataSources.append(contentsOf: dataModels)
                self.tableView.reloadData()
                
            } else {
                // show error aletr
                let alert = UIAlertController.init(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func offlineSwitchChangeValue(sender: UISwitch) {
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        cell.textLabel?.text = dataSources[indexPath.row].desc
        cell.detailTextLabel?.text = dataSources[indexPath.row].site
        
        return cell
    }
}
