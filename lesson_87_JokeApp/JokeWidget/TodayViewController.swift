//
//  TodayViewController.swift
//  JokeWidget
//
//  Created by Yurii Bosov on 1/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import NotificationCenter
import JokeAPIKit

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textLabel?.text = nil
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    @IBAction func reloadDidTap(sender: UITapGestureRecognizer) {
        self.loadJokeModel()
    }
    
    // MARK: - Load Data
    
    func loadJokeModel () {
        self.activityIndicator?.isHidden = false
        self.activityIndicator?.startAnimating()
        
        JokeModel.loadJokes(count: 100) { (data: [JokeModel]?, error: String?) in
            
            self.activityIndicator?.isHidden = true
            self.activityIndicator?.stopAnimating()
            
            if data != nil {
                
                // show content
                let randomIndex = Int(arc4random_uniform(UInt32((data?.count)!)))
                
                self.textLabel?.attributedText = data?[randomIndex].jokeAttributedText()
                
            } else {
                // show error aletr
                self.textLabel?.text = error as String!
            }
        }
    }
    
    // MARK: - NCWidgetProviding
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
        self.loadJokeModel()
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = CGSize(width: 0, height: 110)
        } else {
            
            let size = self.view.systemLayoutSizeFitting(CGSize(width: self.view.frame.size.width, height: 110), withHorizontalFittingPriority: UILayoutPriorityRequired, verticalFittingPriority: UILayoutPriorityFittingSizeLevel)
            
            self.preferredContentSize = CGSize(width: size.width, height: size.height)
        }
    }
}
