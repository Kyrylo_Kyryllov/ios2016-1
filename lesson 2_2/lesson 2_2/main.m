
#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        MyClass *temp = [[MyClass alloc] init];
        NSLog(@"temp = %@", temp);
    }
    return 0;
}
