//
//  NavigationController.h
//  lesson 26_some_storyboards
//
//  Created by Yuriy Bosov on 4/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController

@end
