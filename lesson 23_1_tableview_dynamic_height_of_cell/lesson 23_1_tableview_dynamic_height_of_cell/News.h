#import <Foundation/Foundation.h>

// класс-модель, которая описывает сущность новость
@interface News : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *logo;

@end
