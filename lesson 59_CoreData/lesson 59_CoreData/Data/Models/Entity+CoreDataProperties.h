//
//  Entity+CoreDataProperties.h
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Entity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Entity (CoreDataProperties)

+ (NSFetchRequest<Entity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSNumber *valueInt;

@end

NS_ASSUME_NONNULL_END
