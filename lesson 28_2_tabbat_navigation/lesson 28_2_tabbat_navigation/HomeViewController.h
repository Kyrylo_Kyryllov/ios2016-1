//
//  HomeViewController.h
//  lesson 28_2_tabbat_navigation
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextView *textView;

@end
