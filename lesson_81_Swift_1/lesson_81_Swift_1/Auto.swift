//
//  Auto.swift
//  lesson_81_Swift_1
//
//  Created by Yurii Bosov on 12/17/16.
//  Copyright © 2016 ios. All rights reserved.
//

import Foundation

class Auto : NSObject {
    
    var brandName = ""
    var modelName = ""
    var modelName1: AnyObject = nil
    var fuel = Fuel()
    
    override var description: String {
        return String("name \(modelName), modelName \(modelName), fuel \(fuel.description())")
    }
}
