//
//  ModalViewController.h
//  lesson_72_1_Custom_Modal_Present
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController

@property (nonatomic, weak) UIButton *btn;

@end
