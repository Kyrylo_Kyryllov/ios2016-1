//
//  Flight.h
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Flight : NSObject
{
    NSMutableArray *usersArray;
}

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *time;
@property (nonatomic, readonly) NSUInteger maxCountOfUsers;

- (id)initFlightWithName:(NSString *)name
                    time:(NSString *)time
         maxCountOfUsers:(NSUInteger)maxCountOfUsers;

- (BOOL)canAddUser;
- (void)addUser:(User *)user;
- (NSUInteger)countFreeSeats;

@end
