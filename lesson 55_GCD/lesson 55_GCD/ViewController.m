//
//  ViewController.m
//  lesson 55_GCD
//
//  Created by Yuriy Bosov on 9/16/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"


typedef enum : NSUInteger {
    QUEUE_STATE_NONE,
    QUEUE_STATE_PROCESSING,
    QUEUE_STATE_SUSPEND,
} QUEUE_STATE;


@interface ViewController ()
{
    // очередь для теста
    dispatch_queue_t test_queue;
    // состояние очереди
    QUEUE_STATE queue_state;
}

@property (nonatomic, weak) IBOutlet UIButton* suspendButton;

@end

@implementation ViewController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Difficul
- (void)difficulMethodsWithParam:(id)object {
    
    // start
    CGFloat startTime = CACurrentMediaTime();
    
    for (NSInteger i = 0; i < 1000000000; i++) {
        ;
    }
    
    // end
    CGFloat endTime = CACurrentMediaTime();
    NSLog(@"%@ finished %f", object, endTime - startTime);
    
    // делаем проверку, в каком потоке мы находимся
    if ([NSThread isMainThread]) {
        // если в главном
        [self difficulMethodsEnded];
    } else {
        // если не в главном
        __weak typeof(self) weakSelf = self;
        // update UI (только в главно потоке)
        // dispatch_get_main_queue - возращает очерерь главного потока
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            [weakSelf difficulMethodsEnded];
        });
    }
}

- (void)difficulMethodsEnded {
    
    // рандомно установим цвет для view
    self.view.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
}

#pragma mark - Button Actions

- (IBAction)buttonClicked_main_queue {
    [self difficulMethodsWithParam:@"M"];
}

- (IBAction)buttonClicked_global_queue {
    __weak typeof(self) weakSelf = self;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        [weakSelf difficulMethodsWithParam:@"G"];
    });
}

- (IBAction)buttonClicked_custom_queue:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    // при создании очереди указываем два параметра:
    // 1 - имя очередеи
    // 2 - определяет способ обработки очереди блоков, где:
    // DISPATCH_QUEUE_SERIAL - выполняет последовательно
    // DISPATCH_QUEUE_CONCURRENT - выполняет паралельно
    dispatch_queue_t queue = dispatch_queue_create("com.custom.queue", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_sync(queue, ^{
       [weakSelf difficulMethodsWithParam:@"1"];
    });
    dispatch_sync(queue, ^{
        [weakSelf difficulMethodsWithParam:@"2"];
    });
    dispatch_sync(queue, ^{
        [weakSelf difficulMethodsWithParam:@"3"];
    });
    dispatch_sync(queue, ^{
        [weakSelf difficulMethodsWithParam:@"4"];
    });
    dispatch_sync(queue, ^{
        [weakSelf difficulMethodsWithParam:@"5"];
    });
    dispatch_sync(queue, ^{
        [weakSelf difficulMethodsWithParam:@"6"];
    });
}

- (IBAction)buttonClicked_dispatch_once:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // блок, переданый в dispatch_once вызовется один раз за все время выполнения программы
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        NSLog(@"dispatch once block");
    });
}

- (IBAction)buttonClicked_dispatch_after:(id)sender {
    
    NSLog(@"clicked %f", CACurrentMediaTime());
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    // добавляем блок в очередь, указав времени, через которое его выполнить
    // для этого используем dispatch_after, где
    // dispatch_time_t when - время ожидание, в милисекундах
    
    __weak id weakSelf = self;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW,
                                         5 * NSEC_PER_SEC);
    dispatch_after(time, queue, ^{
        NSLog(@"begin %f", CACurrentMediaTime());
        [weakSelf difficulMethodsWithParam:@"DISPATCH_AFTER"];
    });
}

- (IBAction)buttonClicked_dispatch_suspend:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    // создаем кастомную очередь
    if (!test_queue) {
        test_queue = dispatch_queue_create("com.test.suspend.queue", DISPATCH_QUEUE_SERIAL);
    }
    
    if (queue_state == QUEUE_STATE_NONE) {
        // стартуем блоки в очереди
        queue_state = QUEUE_STATE_PROCESSING;
        [_suspendButton setTitle:@"suspend" forState:UIControlStateNormal];
        
        dispatch_async(test_queue, ^{
            [weakSelf difficulMethodsWithParam:@"1"];
        });
        dispatch_async(test_queue, ^{
            [weakSelf difficulMethodsWithParam:@"2"];
        });
        dispatch_async(test_queue, ^{
            [weakSelf difficulMethodsWithParam:@"3"];
        });
        dispatch_async(test_queue, ^{
            // end
            dispatch_async(dispatch_get_main_queue(), ^{
                if (weakSelf) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    strongSelf->queue_state = QUEUE_STATE_NONE;
                    [weakSelf.suspendButton setTitle:@"start" forState:UIControlStateNormal];
                }
            });
        });
    
    } else if (queue_state == QUEUE_STATE_PROCESSING){
        // ставим на паузу
        queue_state = QUEUE_STATE_SUSPEND;
        dispatch_suspend(test_queue);
        [_suspendButton setTitle:@"resume" forState:UIControlStateNormal];
    } else if (queue_state == QUEUE_STATE_SUSPEND){
        // возобновляем очередь
        queue_state = QUEUE_STATE_PROCESSING;
        dispatch_resume(test_queue);
        [_suspendButton setTitle:@"suspend" forState:UIControlStateNormal];
    }
    
}

@end
